package com.techm.bm.aggrigate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import com.techm.bm.api.cmd.PlaceOrderCmd;
import com.techm.bm.api.cmd.RegisterShipmentForOrderArrivedCmd;
import com.techm.bm.api.cmd.RegisterShipmentForOrderPreparedCmd;
import com.techm.bm.api.evt.OrderPlacedEvt;
import com.techm.bm.api.evt.ShipmentForOrderArrivedEvt;
import com.techm.bm.api.evt.ShipmentForOrderPreparedEvt;

import lombok.NoArgsConstructor;

@Aggregate
@NoArgsConstructor
public class Order {

	@AggregateIdentifier
	private UUID orderId;

	private Set<UUID> undeliveredShipments;

	@CommandHandler
	Order(PlaceOrderCmd cmd) {
		apply(new OrderPlacedEvt(cmd.getOrderId(), cmd.getGoods(), cmd.getDestination()));
	}

	@CommandHandler
	void handle(RegisterShipmentForOrderPreparedCmd cmd) {
		if (!undeliveredShipments.contains(cmd.getShipmentId())) {
			apply(new ShipmentForOrderPreparedEvt(orderId, cmd.getShipmentId()));
		}
	}

	@CommandHandler
	void handle(RegisterShipmentForOrderArrivedCmd cmd) {
		if (undeliveredShipments.contains(cmd.getShipmentId())) {
			apply(new ShipmentForOrderArrivedEvt(orderId, cmd.getShipmentId()));
		}
	}

	@EventSourcingHandler
	void on(OrderPlacedEvt evt) {
		orderId = evt.getOrderId();
		undeliveredShipments = new HashSet<>();
	}

	@EventSourcingHandler
	void on(ShipmentForOrderPreparedEvt evt) {
		undeliveredShipments.add(evt.getShipmentId());
	}

	@EventSourcingHandler
	void on(ShipmentForOrderArrivedEvt evt) {
		undeliveredShipments.remove(evt.getShipmentId());
	}
}
