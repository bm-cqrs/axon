package com.techm.bm.saga;

import static org.axonframework.modelling.saga.SagaLifecycle.associateWith;

import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import com.techm.bm.api.cmd.PrepareShipmentCmd;
import com.techm.bm.api.cmd.RegisterShipmentForOrderArrivedCmd;
import com.techm.bm.api.cmd.RegisterShipmentForOrderPreparedCmd;
import com.techm.bm.api.evt.OrderPlacedEvt;
import com.techm.bm.api.evt.ShipmentArrivedEvt;
import com.techm.bm.api.evt.ShipmentPreparedEvt;

import lombok.extern.slf4j.Slf4j;

@Saga(sagaStore = "sagaStore")
@Slf4j
public class OrderSaga {

	@Autowired
	private transient CommandGateway commandGateway;

	private UUID orderId;

	@StartSaga
	@SagaEventHandler(associationProperty = "orderId")
	public void on(OrderPlacedEvt evt) {
		log.debug("handling {}", evt);
		orderId = evt.getOrderId();
		UUID shipmentId = UUID.randomUUID();
		associateWith("shipmentId", shipmentId.toString());
		commandGateway.send(new PrepareShipmentCmd(shipmentId, evt.getDestination()));
	}

	@SagaEventHandler(associationProperty = "shipmentId")
	public void on(ShipmentPreparedEvt evt) {
		log.debug("handling {}", evt);
		log.debug("orderId: {}", orderId);
		commandGateway.send(new RegisterShipmentForOrderPreparedCmd(orderId, evt.getShipmentId()));
	}

	@SagaEventHandler(associationProperty = "shipmentId")
	@EndSaga
	public void on(ShipmentArrivedEvt evt) {
		log.debug("handling {}", evt);
		log.debug("orderId: {}", orderId);
		commandGateway.send(new RegisterShipmentForOrderArrivedCmd(orderId, evt.getShipmentId()));
	}
}