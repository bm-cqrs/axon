# Order Demo Application using Axon4 framework

###### Order --> Shipment example using Axon Saga
* Saga starts when PlaceOrderCmd is executed
* Shipment is created and gets registered against Order
* Once ShipmentArrivedCmd is executed, end saga is executed


###### Steps to build and deploy

- Start axon server docker instance (alternative - download and run executable server jar from https://axoniq.io/download) 

```
docker run -d --name axon-server -p 8024:8024 -p 8124:8124 axoniq/axonserver
```
- Start Prostgres command database docker instance (alternative - it can be local postgres database)

```
docker run -d --rm --name pg_command -p 5432:5432 -e POSTGRES_USER=orderdemo -e POSTGRES_PASSWORD=secret postgres:latest
```
- Start Prostgres query database (alternative - it can be local postgres database)

```
docker run -d --rm --name pg_query -p 5433:5432 -e POSTGRES_USER=orderdemo -e POSTGRES_PASSWORD=secret postgres:latest
```

- Set docker IP or host IP address as environment variable

```
set AXON_INFRA_HOST_IP=...
```
- Build application

```
mvn clean install
```

- Go to command folder and run command application  

```
mvn spring-boot:run
```

- Go to query and run query application 

```
mvn spring-boot:run
```

- Once command application it deployed, swagger is available:

	 
	http://localhost:9000/
	
- Once query application is deployed, swagger is available:

	http://localhost:9001/

- Axon dashboard is available:

	http://localhost:8024/
