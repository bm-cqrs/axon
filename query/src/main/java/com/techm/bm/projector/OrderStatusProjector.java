package com.techm.bm.projector;

import javax.persistence.EntityManager;

import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import com.techm.bm.api.evt.OrderPlacedEvt;
import com.techm.bm.api.evt.ShipmentForOrderArrivedEvt;
import com.techm.bm.api.evt.ShipmentForOrderPreparedEvt;
import com.techm.bm.view.model.OrderStatus;
import com.techm.bm.view.query.FindOrderStatus;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class OrderStatusProjector {

    private final EntityManager entityManager;

    @EventHandler
    public void on(OrderPlacedEvt evt) {
        entityManager.persist(OrderStatus.builder()
                .orderId(evt.getOrderId())
                .shipmentId(null)
                .delivered(false)
                .build());
    }

    @EventHandler
    public void on(ShipmentForOrderPreparedEvt evt) {
        OrderStatus status = entityManager.find(OrderStatus.class, evt.getOrderId());
        status.setShipmentId(evt.getShipmentId());
    }

    @EventHandler
    public void on(ShipmentForOrderArrivedEvt evt) {
        OrderStatus status = entityManager.find(OrderStatus.class, evt.getOrderId());
        status.setDelivered(true);
    }

    @QueryHandler
    public OrderStatus handle(FindOrderStatus query) {
        return entityManager.find(OrderStatus.class, query.getOrderId());
    }

}
