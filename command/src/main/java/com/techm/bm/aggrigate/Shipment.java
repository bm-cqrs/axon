package com.techm.bm.aggrigate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;
import static org.axonframework.modelling.command.AggregateLifecycle.markDeleted;

import java.util.UUID;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import com.techm.bm.api.cmd.PrepareShipmentCmd;
import com.techm.bm.api.cmd.RegisterShipmentArrivalCmd;
import com.techm.bm.api.evt.ShipmentArrivedEvt;
import com.techm.bm.api.evt.ShipmentPreparedEvt;

import lombok.NoArgsConstructor;

@Aggregate
@NoArgsConstructor
class Shipment {

    @AggregateIdentifier
    private UUID shipmentId;

    @CommandHandler
    Shipment(PrepareShipmentCmd cmd) {
        apply(new ShipmentPreparedEvt(cmd.getShipmentId(), cmd.getDestination()));
    }

    @CommandHandler
    void handle(RegisterShipmentArrivalCmd cmd) {
        apply(new ShipmentArrivedEvt(shipmentId));
    }

    @EventSourcingHandler
    void on(ShipmentPreparedEvt evt) {
        shipmentId = evt.getShipmentId();
    }

    @EventSourcingHandler
    void on(ShipmentArrivedEvt evt) {
        markDeleted();
    }

}
